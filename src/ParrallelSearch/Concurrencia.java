package ParrallelSearch;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;

public class Concurrencia {
	protected static int index=-1;
	static int hilo;
	static int search;
	static int tam;

	
	public static int parallelSearch(int x, int A[], int numThreads)
	{
		//Dividir el arreglo
		//length -->Para encontrar el tama�o de la matriz
		int divisionArreglo = A.length/numThreads;
		Thread hilos[]= new Thread[numThreads];
		int fin=0;
		int inicio=0;
		
		System.out.println("\n\nBusqueda de: "+x);
		
		//Arrays.toString imprime como cadena el contenido del arreglo
		System.out.println("Array: "+Arrays.toString(A));
		
		//Busqueda
		for(int i=0; i<numThreads; i++)
		{
			fin=inicio+divisionArreglo;
			if(i == numThreads-1 || fin>A.length)
			{
				fin = A.length;
			}
			Search objetivo = new Search(inicio, fin, A, x);
			hilos[i] = new Thread(objetivo);
			hilos[i].start();
			inicio=fin;
		}
		return index;
	}
	
	public static void main(String[] args) throws NumberFormatException, IOException
	{
		InputStreamReader isr = new InputStreamReader(System.in);
		BufferedReader br = new BufferedReader (isr);
		
		System.out.print("Tama�o del arreglo: ");
		tam = Integer.parseInt( br.readLine () );
		
		int arreglo[] = new int[tam];
		for(int i=0; i<arreglo.length; i++)
		{
			//Crear areglo con n�meros aleatorios
			arreglo[i]=(int)(Math.random()*20);	
		}
		
		int aleatorio;
		aleatorio=(int)(Math.random()*20+1);
		
		System.out.print("N�mero de hilos: ");
		hilo = Integer.parseInt( br.readLine () );
		
		System.out.print("Valor a buscar: ");
		search = Integer.parseInt( br.readLine () );

		
		int parallelSearch = parallelSearch(search,arreglo,hilo);
		if(parallelSearch == -1)
		{
			System.out.println("");
		}
		
	}

}

class Search extends Concurrencia implements Runnable
{
	int inicio, fin, x;
	int A[];
	
	public Search(int inicio, int fin, int A[], int x)
	{
		super();
		this.inicio=inicio;
		this.fin=fin;
		this.A=A;
		this.x=x;
	}
	
		@Override
		public void run()
		{
			for(int i = inicio; i<fin; i++)
			{
				
				if(x==A[i])
				{
					index=i;
					
					System.out.print("\nEncontrado en posicion: "+i+"\nHilo en ejecuci�n: "+Thread.currentThread().getName());
							
				}
				else
				{
					System.out.println("\nNo encontrado \nHilo en ejecuci�n: "+Thread.currentThread().getName());
				}
				
			}
		}
		
	}
