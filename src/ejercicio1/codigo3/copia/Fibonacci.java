package ejercicio1.codigo3.copia;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Fibonacci extends Thread
{
	int n;
	int result;    
	static int id;
	
	//Nuevo m�todo
	public int getNumber() 
	{ return n;}
	
	public Fibonacci( int n )
	{ this.n = n; }
	
	public void run()
	{
		if( n == 0 || n == 1 )
		{
			result = 1;
		}
		else
		{
			Fibonacci f1 = new Fibonacci( n-1 );
			Fibonacci f2 = new Fibonacci( n-2 );
			System.out.printf("\nID Padre: %d N�mero: %d\nID Hijo1: %d N�mero: %d\nID Hijo2: %d N�mero: %d", this.getId(), this.getNumber(), f1.getId(), f1.getNumber(), f2.getId(), f2.getNumber());
			f1.start();
			f2.start();
			try{
			f1.join();
			f2.join();
			
			}catch( InterruptedException e ){};
			
			result = f1.getResult() + f2.getResult();
			System.out.printf("\n\nID Padre: %d\nResultado: %d = %d(Id:%d) + %d(Id:%d)",this.getId(), result, f1.getResult(), f1.getId(), f2.getResult(), f2.getId());
			
		}
	}
	
	public int getResult()
	{
		return result;
	}
	
	
	public static void main( String[] args ) throws NumberFormatException, IOException
	{
		System.out.print("Escribe el n�mero de fibonacci que deseas obtener: ");
		InputStreamReader isr = new InputStreamReader(System.in);
		BufferedReader br = new BufferedReader (isr);
		
		id = Integer.parseInt( br.readLine () );
		
		Fibonacci f = new Fibonacci( id );
		f.start();
		try{
		f.join();
		}catch( InterruptedException e ){};
		System.out.println( "\n\nAnswer is " + f.getResult() );
	}
}

