package ejercicio1.codigo1.copia;

public class HelloWorld extends Thread
{
	int pid;
	String nom; 
	Thread t;
	
	//constructor
	public HelloWorld(String nombre)
	{
		nom = nombre;
		pid=(int) this.getId();
	}
	
	//Modificar mensaje
	public void run()
	{
		/*t=new Thread(t,"Paco");
		t=new Thread();
		t.setName("Paco");
		t.start();
		System.out.println("Name: " +t.getName());
		System.out.println("ID: " + t.getId());
		*/
		System.out.printf("\nName: %s",nom);
		System.out.printf("\nID: %d",pid);
	}
	
	//Arreglo de objetos
	public static void main(String[] args)
	{
		HelloWorld arreglo[] = new HelloWorld[3];
		arreglo[0] = new HelloWorld("Obj1");
		arreglo[0].start();
		try {
			arreglo[0].join();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		arreglo[1] = new HelloWorld("Obj2");
		arreglo[1].start();
		try {
			arreglo[1].join();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		arreglo[2] = new HelloWorld("Obj3");
		arreglo[2].start();
		try {
			arreglo[2].join();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
